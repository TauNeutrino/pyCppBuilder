import io
import os


class Git:
    """ Setup the git environment for your new repo """

    def __init__(self, git_repo_url='', git_branch='', git_user='', git_root_project_dir='', git_project_dir='', git_repo_name=''):
        self.git_user = git_user
        self.git_repo_url = git_repo_url
        self.git_branch = git_branch
        self.git_root_project_dir = git_root_project_dir
        self.git_project_dir = git_project_dir
        self.git_repo_name = git_repo_name

    def debug_print_values(self):  # Use to debug class values
        t = term_colours()
        print(t.OKGREEN + '---' + t.OKGREEN)
        print(t.HEADER + 'Values in this instance:' + t.ENDC)
        print('gituser is:\t\t'+t.OKGREEN+'{0}'.format(self.git_user) + t.ENDC)
        print('git_repo_url is:\t'+t.OKGREEN +
              '{0}'.format(self.git_repo_url) + t.ENDC)
        print('git_branch is:\t\t'+t.OKGREEN +
              '{0}'.format(self.git_branch) + t.ENDC)
        print('git_repo_name is:\t'+t.OKGREEN +
              '{0}'.format(self.git_repo_name) + t.ENDC)
        print('git_root_project_dir:\t'+t.OKGREEN +
              '{0}'.format(self.git_root_project_dir) + t.ENDC)
        print('git_project_dir is:\t'+t.OKGREEN +
              '{0}'.format(self.git_project_dir) + t.ENDC)
        print(t.OKGREEN + '---' + t.ENDC)


class term_colours:
    """ Use colours to make terminal pretty. From:
    https://svn.blender.org/svnroot/bf-blender/trunk/blender/build_files/scons/tools/bcolors.py"""
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

    def disable(self):
        self.HEADER = ''
        self.OKBLUE = ''
        self.OKGREEN = ''
        self.WARNING = ''
        self.FAIL = ''
        self.ENDC = ''
# --- EOF ---
