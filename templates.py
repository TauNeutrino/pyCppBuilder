import os
# Edit these templates to suite your needs
# or add new ones folowing these as an example
# The README.md must be created after the git init command


def write_HDR_template(g):
    """Write a template to the header file specified"""
    path = g.git_project_dir + "/hdr"
    os.chdir(path)
    file_name = g.git_repo_name + '.hpp'
    hdr_name_lc = g.git_repo_name + '_HPP'
    # Change to upper case
    hdr_name_uc = hdr_name_lc.upper()
    # Write header gaurd
    f = open(file_name, 'a')
    f.write("//-------------------------------------------\n")
    f.write("//" + hdr_name_uc + "\n")
    f.write("//-------------------------------------------\n")
    f.write("#ifndef {0}\n".format(hdr_name_uc))
    f.write("#define {0}\n\t\n\t\n".format(hdr_name_uc))
    f.write("\tYour code goes here")
    f.write("#endif // {0}\n".format(hdr_name_uc))
    f.close()


def write_main_template(g):
    """Write a template to the header file specified"""
    path = g.git_project_dir + "/src"
    os.chdir(path)
    file_name = 'main.cpp'
    f = open(file_name, "a")
    # Edit this template to suite your needs
    f.write("//-------------------------------------------\n")
    f.write("// Project " + g.git_repo_name + "\n")
    f.write("//-------------------------------------------\n")
    f.write("#include <cstdlib>\n#include <iostream>\n")
    # Comment out this line if you do not need a project header
    f.write("#include \"../hdr/" + g.git_repo_name + ".hpp\"\n")
    f.write(
        "int main(int argc, char const *argv[]){ \n\t//-=Code=-\n\n\treturn 0;\n}//main()\n\nEOF\n")
    f.close()


def modify_makefile(g, this_app_dir):
    """ Create a new Makefile from the template makefile-template """
    # Copy makefile-template to makefile.tmp
    os.chdir(this_app_dir)
    os.system("cp makefile-template makefile.tmp")
    # Create makefile with TARGET = (git_repo_name)
    makefile_change = open("makefile.tmp", "r")
    lines = makefile_change.readlines()
    # Change TARGET name to project name
    lines[7] = "TARGET\t:= " + g.git_repo_name + "\n"
    makefile_change = open("makefile.tmp", "w")
    makefile_change.writelines(lines)
    makefile_change.close()
    # Move new makefile to project directory
    shell_statement = "mv makefile.tmp " + g.git_project_dir + "/makefile"
    print("---")
    print("moving makefile.tmp to project directory")
    os.system(shell_statement)
    # remove makefile.tmp
    os.system('rm makefile.tmp')


def write_README_template(g, this_app_dir):
    """ Create a README.md file for your project """
    os.chdir(g.git_project_dir)
    f = open("README.md", "a")
    f.write("# " + g.git_repo_name + "\n")
    f.write("## DESCRIPTION\n")
    f.write("### By: " + g.git_user + "\n")
    f.write("- This project's repository: [" + g.git_repo_url + "]\n")
    f.write("- Edit this README\n")
    f.write("\n")
    f.write("--- \n")
    f.write("\n")
    f.write("Directory Tree\n")
    f.write("<pre>\n")
    # Write dir tree into end of README.md
    f.close()
    os.system("tree --dirsfirst --charset=ascii " +
              g.git_project_dir + " >> README.md")
    f = open("README.md","a")
    f.write("</pre>")
    f.close()
   
# --- EOF ---