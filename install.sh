#!/bin/bash
# Make this executable with chmod +x install.sh  and execute with: ./install.sh'

clear

# Modify config.ini
function ModifyConfig(){
    local user=$(git config user.name)
    local key='git_user'
    local file='config.ini'
    # change git user name 
    sed -i "s/\($key *= *\).*/\1$user/" $file
    echo 'Config file modified with your git user name'
}

echo 'Your PythonPIP3 Version'
pip3 --version
echo
echo 'Installing configparser: '
pip3 install configparser
echo
ModifyConfig
echo
echo 'Add tree to your system'

if command -v apt-get >/dev/null; then
  sudo apt install -y tree
elif command -v yum >/dev/null; then
  sudo yum install tree
else
  echo "Could not find apt or yum, install tree using your package manager"
fi
ls *

