# pyCppBuilder
### Python scripts to automate C/C++ project creation

#### Linux only - Not for use on Windows [^1]

Create an empty repository on GitHub [^2]

- Installation
  - ```git clone https://github.com/ve6ji-code/pyCppBuilder.git```
  - ```cd pyCppBuilder/```
  - ```pip3 install configparser``` [^3]
  - or ```chmod +x install.sh && ./install.sh```
- Use
  - ```/usr/bin/python3 pyCppBuilder.py```
  - Paste your empty repo ex. ```git@github.com:(your_user_name)/(your_repo).git```

- Tasks handled by this app
  - Setup git
  - Create a directory structure
  <pre>
    /home/your/projects/git-repo/
                                |--.git/
                                |      -- git stuff
                                |-- hdr/
                                |      -- (repo_name).hpp
                                |-- src/
                                |      -- main.cpp
                                |-- makefile
                                |-- README.md
  </pre>
  
  - Create a main.cpp file in /src
  - Create a (REPO-Name).hpp file in /hdr
  - Place a makefile in home/your/projects/git_repo/
  - Run git commands
    - ```git init```
    - Place a README.MD in ```home/your/projects/git_repo/```
    - ```git add .```
    - ```git commit -m "initial commit"```
    - ```git remote add origin git@github.com:(your_user_name)/(your_repo.git)```
    - ```git branch -M (your_branch)```
    - ```git remote -v```
    - ```git -u push -origin```


[^1]:  Posix based systems only.  [Please see issues](https://github.com/ve6ji-code/pyCppBuilder/issues) to fork a windows capable version.
[^2]: [GitHub Instructions](https://docs.github.com/en/get-started/quickstart/create-a-repo)
[^3]:  [pip3 rescources](https://packaging.python.org/en/latest/tutorials/installing-packages/)
