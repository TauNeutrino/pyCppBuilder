import warnings
import pwd
import io
import os
import sys
import re
from git import *
from templates import *
import configparser


def main():
    """Setup git environment"""
    this_app_dir = os.getcwd()
    g = Git()
    setup(g)
    # Change makefile-template and move to project directory
    modify_makefile(g, this_app_dir)
    # Populate src with main.cpp template
    write_main_template(g)
    # Populate hdr with (repo).hpp template
    write_HDR_template(g)
    # Init local Git repo
    os.chdir(g.git_project_dir)
    git_init = "git init"
    os.system(git_init)
    # Write README.md template
    write_README_template(g, this_app_dir)
    # Add all of the files to the repo
    os.system("git add .")
    # Commit
    os.system("git commit -m \"initial commit\"")
    # Setup remote repo
    git_remote = "git remote add origin " + g.git_repo_url
    os.system(git_remote)
    git_branch = "git branch -M" + g.git_branch
    # Verify remote repot
    os.system("git remote -v")
    # Push to the remote GitHubrepo
    git_push = "git push -u origin " + g.git_branch
    os.system(git_push)
    # --- End of main ---


def setup(g):
    # read from config.ini
    config = configparser.ConfigParser()
    config.read('config.ini')
    git_config = config['git']
    g.git_user = git_config['git_user']
    g.git_branch = git_config['git_branch']
    g.git_root_project_dir = git_config['root_project_dir']

    # Get repo details
    os.system('clear')  # Clear terminal
    user = pwd.getpwuid(os.getuid())[0]  # Get user name from system
    t = term_colours()
    print(t.OKGREEN + '---' + t.OKGREEN)
    print(t.HEADER + 'C++ Project Builder' + t.ENDC)
    print('{0}'.format(user) + ', please paste your ' +
          t.WARNING + 'EMPTY' + t.ENDC + ' GitHub URI')
    print("Example: git@github.com:tauneutrino-code/test.git")
    g.git_repo_url = input("Empty Repo: ")

    # Confirm, or change, default values in config.ini
    # User name
    response = input(
        '{0} is the default git user. is this ok? (y/n) '.format(g.git_user)) or 'y'
    validate_input(response)
    if response == 'n' or response == 'N':
        # Change default user name
        new_git_user = input('New default user name: ')
        g.git_user = new_git_user
        set_value_config('config.ini', 'git', 'git_user', g.git_user)

    # Branch
    response = input(
        '{0} is the default git branch.  is this ok? (y/n) '.format(g.git_branch)) or 'y'
    validate_input(response)
    if response == 'n' or response == 'N':
        # Change default branch name
        new_git_branch = input('New default branch: ')
        g.git_user = new_git_branch
        set_value_config('config.ini', 'git', 'git_branch', g.git_branch)

    # Root project directory
    response = input(
        '{0} is the default project root directory. is this ok? (y/n) '.format(g.git_root_project_dir)) or 'y'
    validate_input(response)
    if response == 'n' or response == 'N':
        # Change default root directory
        new_project_dir = input('New root project dir: ')
        g.git_root_project_dir = new_project_dir
        set_value_config('config.ini', 'git',
                         'root_project_dir', g.git_root_project_dir)
        build_dir(g, config)
    else:
        # Create project dir @ root directory
        build_dir(g, config)


def build_dir(g, config):
    """ Build the Directory structure for your git repo """
    # Extract repo name to use as the directory
    print("---")
    s = str(g.git_repo_url)
    split_1 = s.split("/")
    split_2 = split_1[1].split(".")
    repo = str(split_2[0])
    # Build dir
    new_dir = str(g.git_root_project_dir + '/' + repo)
    print('The full directory is {0}'.format(new_dir))
    g.git_project_dir = new_dir
    g.git_repo_name = repo
    g.debug_print_values()
    # Create directory with os.makedirs
    path = os.path.join(g.git_root_project_dir, repo)
    os.chdir('/')
    os.makedirs(path)
    # Change to new project directory
    os.chdir(path)
    # Create headers directory
    os.mkdir('hdr')
    # Create Source file directory
    os.mkdir('src')


def validate_input(user_input):
    """ Test for yes and no inputs """
    if user_input == 'n' or user_input == 'N' or user_input == 'y' or user_input == 'Y':
        response = user_input
        return response
    else:
        response = input('Invalid input.  Please enter y or n ')
        validate_input(response)


def set_value_config(file_path, section, key, value):
    """ Update config.in with new values """
    config = configparser.RawConfigParser()
    config.read(file_path)
    config.set(section, key, value)
    cfgfile = open(file_path, 'w')
    # use flag in case case you need to avoid white space.
    config.write(cfgfile, space_around_delimiters=False)
    cfgfile.close()


if __name__ == "__main__":
    """Main Function"""
    main()
# --- EOF ---Usage: grep [OPTION]... PATTERNS [FILE]...
